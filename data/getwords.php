<?php
$db = new PDO('sqlite:base/words_base');

$st = $db->query('SELECT id,word,translation,cat_id,level FROM words');
$result = $st->fetchAll(PDO::FETCH_ASSOC);
$response = json_encode($result);
header('Content-Type: application/json');
print $response;