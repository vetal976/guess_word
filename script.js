let words, secret, entered, riddle, guessedWords = [];
init();

/* начальная инициализация */
function init(){
	riddle = [], entered = [];//масивы звездочек и введенных пользователем символов 
	if (words == undefined){//если массив слов еще не инициализирован
		fetch('/data/getwords.php')//делаем запрос к серверу (базе данных) 
			.then(function(response){
				return response.json();
			})
			.then(function(wordsFromServer){
				words = wordsFromServer;
				console.log("words were received from server");
				selectWord();
			})
			.catch(alert);
	} else {
		selectWord();
	}
	function selectWord(){
		let wordsNumber = words.length;//определяем количество слов в массиве
		secret = words[Math.ceil(Math.random()*wordsNumber) - 1];//выбираем слово из массива
		showRiddle();//отображаем звездочки по количеству букв			
		document.getElementById("translation").innerHTML = secret.translation;/* показываем перевод
		и картинку */
		document.getElementsByTagName("img")[0].setAttribute("src","/data/jpgs/word_" + secret.id + ".jpg");	
	}
	document.getElementById("entered").innerHTML = "";
	document.getElementById("msg").innerHTML = "Guess the word";//отображаем сообщение	
	document.getElementsByName("letter")[0].focus();//делаем кнопку "Try" активной		
}

/* отображение *** вместо букв */
function showRiddle(){
	if (riddle.length == 0){
		riddle = Array(secret.word.length);//массив *** вместо букв
		for (let i = 0, l = riddle.length; i < l; i++){//заполнение массива ***-ми
			riddle[i] = "*";
		}			
	}
	let riddleStr = riddle.join("");
	document.getElementById("riddle").innerHTML = riddleStr;
	if (riddleStr.indexOf("*") == -1) { //если все буквы отгаданы
		guessedWords.push(secret.word);//добавляем отгаданное слово в соотв. массив
		document.getElementById("guessedWords").innerHTML = guessedWords.join(", ");//отображаем массив отгаданных слов в виде строки
		document.getElementById("msg").innerHTML = "Correct!";//отображаем сообщение
		updateItem({id: secret.id, level: parseInt(secret.level) + 1});	//обновление поля level в базе данных
		//делаем кнопки "Try" и "Show answer" не активными
		document.getElementsByTagName("button")[0].setAttribute("disabled", true);
		document.getElementsByTagName("button")[2].setAttribute("disabled", true);
		document.getElementsByTagName("button")[1].focus();	//делаем кнопку "New word" активной
	}	
}

/* проверка присутствия в слове подстроки (буквы), введенной пользователем */
function checkLetterExistance(letter){	
	if (!entered.includes(letter)) {//если буква (набор букв) еще не вводился,
		entered.push(letter);//добавляем ее в соотв. массив,
		entered.sort();//сортируем его для отображения в алфавитном порядке,
		document.getElementById("entered").innerHTML = entered.join();//отображаем,
		let index = secret.word.indexOf(letter);//ищем первое вхождение
		while (index != -1){//если присутствует
			//заменяем звездочки
			for (let i = 0, l = letter.length; i < l; i++, index++){
				riddle[index] = letter[i];
			}			
			index = secret.word.indexOf(letter, index);//ищем следующие вхождения
		}		
	}
	document.getElementsByName("letter")[0].focus();//делаем кнопку "Try" активной	
}

/* -------------------- event handlers ---------------------------- */
/* "Try" button */
document.getElementsByTagName("button")[0].addEventListener("click", function(){
	tryButtonHandler();
});

function tryButtonHandler(){
	let letter = document.getElementsByName("letter")[0].value;
	if (letter != " " ) letter = letter.trim();//если введен не пробел, обрезаем
	if (letter.search(/[0-9А-Яа-я_\.,+=*?\\\/!;:#$@()]\s/) != -1 ) {
		letter = "";
		//console.log("недопустимый символ!");
	}
	if (letter != ""){
	//if (letter.match(/[A-Za-z-]*\s/)){	
		checkLetterExistance(letter);
		showRiddle();
	} else {
		alert("You must enter the letter!");
	}
	document.getElementsByName("letter")[0].value = "";
}

/* Enter key pressed */
window.addEventListener("keypress", function(e){
	//console.log(e);
	if(e.keyCode == 13 && e.target.name == "letter") tryButtonHandler();
});

/* "New word" button */
document.getElementsByTagName("button")[1].addEventListener("click", function(){
	document.getElementsByTagName("button")[0].removeAttribute("disabled");
	document.getElementsByTagName("button")[2].removeAttribute("disabled");	
	init();	
});

/* "Show answer" button */
document.getElementsByTagName("button")[2].addEventListener("click", function(){
	document.getElementsByTagName("button")[0].setAttribute("disabled", true);
	document.getElementsByTagName("button")[2].setAttribute("disabled", true);
	document.getElementById("riddle").innerHTML = secret.word;
});

/* "Update" button */
document.getElementsByTagName("button")[3].addEventListener("click", function(){
	let examples = document.getElementById("examples").value;
	if (examples != "") updateItem({id: secret.id, cat_id: document.getElementsByTagName("select")[0].value, examp: examples});
});

/* обновление полей  level или category и examples в таблицах базы данных */
function updateItem(data){
	fetch('/data/update.php',{
		credentials: 'same-origin',
		method: 'POST',
		body: JSON.stringify(data),
		headers: new Headers({
			'Content-Type': 'application/json'
		}),
	})
	.then(response=>response.text())//response.json()
	.catch(alert);
}